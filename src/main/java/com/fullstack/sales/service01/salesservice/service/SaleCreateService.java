package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.input.SaleInput;
import com.fullstack.sales.service01.salesservice.model.domain.Sale;
import com.fullstack.sales.service01.salesservice.model.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class SaleCreateService {
    private SaleInput saleInput;

    @Autowired
    private SaleRepository saleRepository;

    public Sale save() {
        return saleRepository.save(composeSaleInstance());
    }

    private Sale composeSaleInstance() {
        Sale instance = new Sale();
        instance.setIcreatedDate(saleInput.getIcreatedDate());
        instance.setNumberSale(saleInput.getNumberSale());
        return  instance;
    }

    public SaleInput getSaleInput() {
        return saleInput;
    }

    public void setSaleInput(SaleInput saleInput) {
        this.saleInput = saleInput;
    }
}
