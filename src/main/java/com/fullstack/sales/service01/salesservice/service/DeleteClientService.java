package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Gender;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class DeleteClientService {

    private Gender  gender;

    private Long idClient;

    private Client client;

    @Autowired
    ClientRepository clientRepository;


    public void execute(){
        deleteClient(idClient,gender);
    }

    public void deleteClient(Long id,Gender gender) {
        if(clientRepository.existsById(id)) {
            Optional<Client> clientInformation = findClientlById(id);
            clientInformation.get().setGender(gender);
            clientRepository.save(clientInformation.get());

        }

        else
        {
            System.out.println("No exist the client with id '" + id + "' for update.");
        }
       /* Optional<Client> clientInformation = findClientlById(id);
        System.out.print("valor ide " + clientInformation);
        clientInformation.get().setDeleted(Boolean.TRUE);
        clientRepository.save(clientInformation.get());
        */
    }




    public Optional<Client> findClientlById(Long id) {
        return clientRepository.findById(id);
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
