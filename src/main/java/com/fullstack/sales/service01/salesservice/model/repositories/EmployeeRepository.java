package com.fullstack.sales.service01.salesservice.model.repositories;

import com.fullstack.sales.service01.salesservice.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}

