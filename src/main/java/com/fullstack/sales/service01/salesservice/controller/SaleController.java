package com.fullstack.sales.service01.salesservice.controller;

import com.fullstack.sales.service01.salesservice.input.EmployeeInput;
import com.fullstack.sales.service01.salesservice.input.SaleInput;
import com.fullstack.sales.service01.salesservice.model.domain.Employee;
import com.fullstack.sales.service01.salesservice.model.domain.Sale;
import com.fullstack.sales.service01.salesservice.service.DeleteSaleService;
import com.fullstack.sales.service01.salesservice.service.EmployeeCreateService;
import com.fullstack.sales.service01.salesservice.service.SaleCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Api(
        tags = "Sale rest",
        description = "Operations over Sale"
)
@RestController
@RequestMapping("/Sale")
@RequestScope
public class SaleController {

    @Autowired
    private SaleCreateService saleCreateService;

    @Autowired
    private DeleteSaleService deleteSaleService;

    @ApiOperation(
            value = "Endpoint to create Sale"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create client"
            ),
            @ApiResponse(
                    code = 404,
                    message = "Not fount test"
            )
    })

    @RequestMapping(method = RequestMethod.POST)
    public Sale createSale(@RequestBody SaleInput input) {
        saleCreateService.setSaleInput(input);
        return saleCreateService.save();
    }

    @ApiOperation(value = "Delete Client")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public Sale removeEmployee(@RequestParam Long id){
        deleteSaleService.setIdSale(id);
        deleteSaleService.execute();
        return deleteSaleService.getSale();
    }


}
