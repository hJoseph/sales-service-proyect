package com.fullstack.sales.service01.salesservice.model.domain;

/**
 * @Autor Henry Joseph Calani A.
 **/
public enum Gender {
    MALE,
    FEMALE
}
