package com.fullstack.sales.service01.salesservice.service;

import com.fullstack.sales.service01.salesservice.model.domain.Client;
import com.fullstack.sales.service01.salesservice.model.domain.Sale;
import com.fullstack.sales.service01.salesservice.model.repositories.ClientRepository;
import com.fullstack.sales.service01.salesservice.model.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Scope("prototype")
@Service
public class DeleteSaleService {

    private Long idSale;

    private Sale sale;

    @Autowired
    SaleRepository saleRepository;


    public void execute(){

        deleteSale(idSale);
    }

    public void deleteSale(Long idSale) {

        saleRepository.deleteById(idSale);
    }

    public Long getIdSale() {
        return idSale;
    }

    public void setIdSale(Long idSale) {
        this.idSale = idSale;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }
}
