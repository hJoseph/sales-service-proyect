package com.fullstack.sales.service01.salesservice.input;

import java.util.Date;

/**
 * @Autor Henry Joseph Calani A.
 **/
public class SaleInput {
    private Long id;
    private Long numberSale;
    private Date icreatedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getIcreatedDate() {
        return icreatedDate;
    }

    public void setIcreatedDate(Date icreatedDate) {
        this.icreatedDate = icreatedDate;
    }
}
