package com.fullstack.sales.service01.salesservice.model.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * @Autor Henry Joseph Calani A.
 **/
@Data
@Entity
@Table(name = "employee_table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid", referencedColumnName = "personid")
})
public class Employee extends Person{

    @Column(name = "position", length = 50, nullable = false)
    private String position;
}
