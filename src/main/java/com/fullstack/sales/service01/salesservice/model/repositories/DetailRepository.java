package com.fullstack.sales.service01.salesservice.model.repositories;

import com.fullstack.sales.service01.salesservice.model.domain.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Autor Henry Joseph Calani A.
 **/
public interface DetailRepository extends JpaRepository<Detail, Long> {
}
